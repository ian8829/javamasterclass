package com.timbuchalka;

public class Char {
    public static void main(String[] args) {
        char myChar = '\u00A9';
        System.out.println("Unicode output was " + myChar);

        boolean myBoolean = true;
        boolean isMale = false;
        System.out.println("myBoolean? " + myBoolean);
    }
}

